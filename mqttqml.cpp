#include "mqttqml.h"

MqTTQML::MqTTQML(const QString &hostName, quint16 port, QObject *parent) :
    QMQTT::Client(hostName, port, false, true, parent)
{
    QHostInfo::lookupHost(hostName, this, SLOT(onLookupFinished(QHostInfo)));
    connect(this, &MqTTQML::published, this, &MqTTQML::onPublished);
    connect(this, &MqTTQML::received, this, &MqTTQML::onReceived);

    //setHost(hostName);
    QMQTT::Client::setClientId("");
    connectToHost();
}

void MqTTQML::onLookupFinished(const QHostInfo &host)
{
    if (host.error() != QHostInfo::NoError) {
        qDebug() << "Lookup failed:" << host.errorString();
        return;
    }

    foreach (const QHostAddress &address, host.addresses())
        qDebug() << "Found address:" << address.toString();
//    if (host.addresses().count() > 0) {
//        setHost(host.addresses().first());
//        connectToHost();
//    }
}

void MqTTQML::onPublished(const QMQTT::Message &message)
{
    emit publishedMessage(message.topic(), QString(message.payload()));
}

void MqTTQML::onReceived(const QMQTT::Message &message)
{
    emit receivedMessage(message.topic(), QString(message.payload()));
}

void MqTTQML::publishMessage(quint16  id, const QString &topic, const QString &message)
{
    QMQTT::Message _message(id, topic, message.toUtf8());
    QMQTT::Client::publish(_message);
}

void MqTTQML::setCredentials(const QString &username, const QString &password)
{
    QMQTT::Client::setUsername(username);
    QMQTT::Client::setPassword(password);
}
