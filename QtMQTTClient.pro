TEMPLATE = app

QT += qml quick network

INCLUDEPATH += qmqtt

SOURCES += main.cpp \
    mqttqml.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(qmqttlib/qmqtt.pri)

HEADERS += \
    mqttqml.h
