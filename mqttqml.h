#ifndef MQTTSENDER_H
#define MQTTSENDER_H

#include <QObject>
#include <QHostInfo>
#include "qmqttlib/qmqtt.h"

class MqTTQML : public QMQTT::Client
{
    Q_OBJECT
public:
    explicit MqTTQML(const QString &hostName, quint16 port, QObject *parent = 0);
    virtual ~MqTTQML() {}
private slots:
    void onLookupFinished(const QHostInfo &host);
    void onPublished(const QMQTT::Message &message);
    void onReceived(const QMQTT::Message &message);
signals:
    void publishedMessage(const QString &topic, const QString &message);
    void receivedMessage(const QString &topic, const QString &message);
public slots:
    void publishMessage(quint16 id, const QString &topic, const QString &message);
    void setCredentials(const QString &username, const QString &password);
private:
};

#endif // MQTTSENDER_H
