import QtQuick 2.0
import QtQuick.Controls 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("MQTT test app")
    property int msgcounter: 1
    //property string commonTopic: "vcctopic"
    property string commonTopic: "/BCC/TEMP"

    Connections {
        target: Sender
        onError: {
            console.log("MQTT sender connection error: ", error)
        }

        onConnected: {
            console.log("MQTT sender connected")
            Sender.subscribe(commonTopic, 0)
        }
        onSubscribed:  {
            console.log("sender subscribed to:", topic)
            root.title = root.title + " subscribed to: " + topic
            Sender.publishMessage(msgcounter++, commonTopic, "Test message")
        }
        onPublishedMessage: {
            console.log("published", message)
        }
    }

    Connections {
        target: Receiver
        onError: {
            console.log("MQTT receiver connection error: ", error)
        }
        onConnected: {
            console.log("MQTT receiver connected")
            Receiver.subscribe(commonTopic, 0)
        }
        onSubscribed:  {
            console.log("receiver subscribed to:", topic)
        }
        onReceivedMessage: {
            console.log("received", message)
            receivedMessagesModel.append({"message": message})
        }
    }
    TextField {
        id: textField
        width: parent.width
        onAccepted: {
            Sender.publishMessage(msgcounter++, commonTopic, text)
        }
    }
    ListModel {
        id: receivedMessagesModel
    }

    ListView {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: textField.bottom
        anchors.bottom: parent.bottom

        model: receivedMessagesModel
        delegate: Text {
            text: message
        }
    }

    Component.onCompleted: {
//        Sender.setUsername("admin")
//        Sender.setPassword("password")
//        Receiver.setUsername("admin")
//        Receiver.setPassword("password")
    }
}
