#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QNetworkProxyFactory>
#include <QQmlContext>

#include "mqttqml.h"

//how to setup Mosquitto server:
//https://sivatechworld.wordpress.com/2015/06/11/step-by-step-installing-and-configuring-mosquitto-with-windows-7/
//cep.test.se.volvocars.com
//const QString EXAMPLE_HOST = "cepsig.test.se.volvocars.com";
//const QString EXAMPLE_HOST = "cep.eu.vcctest.ericssoncvc.com";
//const QString EXAMPLE_HOST = "test.mosquitto.org";//85.119.83.194
//const QString EXAMPLE_HOST = "85.119.83.194";

/*
 * SETUP for Stunnel:
 *
 * [stunnel-mqtt-8883]
 * accept = 8883
 * protocol = socks
 * connect = cep.eu.vcctest.ericssoncvc.com:8883
 * client = yes
 * cert = <YOUR_CERT_FILE>.crt
 * key = <YOUR_KEY_FILE>.key
 */

const QString EXAMPLE_HOST = "127.0.0.1";
const quint16 EXAMPLE_PORT = 8883;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    //QNetworkProxyFactory::setUseSystemConfiguration(false);
    //QNetworkProxy proxy(QNetworkProxy::Socks5Proxy);
    //proxy.setHostName("cep.eu.vcctest.ericssoncvc.com");
    //proxy.setPort(8883);
    //QNetworkProxy::setApplicationProxy(proxy);
    MqTTQML sender(EXAMPLE_HOST, EXAMPLE_PORT);
    MqTTQML receiver(EXAMPLE_HOST, EXAMPLE_PORT);
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("Sender", &sender);
    engine.rootContext()->setContextProperty("Receiver", &receiver);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
